import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        int[] vector = {1, 3, 2, 1, 4, 5, 4, 7, 1, 3, 2};
        Histogram histogram = new Histogram(vector);

        HashMap<Integer, Integer> histogr = histogram.getHisto();

        for (Integer key : histogr.keySet()) {
            System.out.println(key + "-->" + histogr.get(key));
        }
    }
}
